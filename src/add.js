﻿import {inject} from 'aurelia-framework';
import {PlantService} from './plantService';
import {Router} from 'aurelia-router';

@inject(PlantService, Router)
export class AddPlant {
    
    heading = 'Add Plant';

    plant = {
        imageUrl: '',
        commonName: '',
        family: '',
        genus: ''
    };

    constructor(plantService, router) {
        this.plantService = plantService;
        this.router = router;
    }

    add() {
        this.plantService.add(this.plant);
        this.router.navigate('plants');
    }
}