﻿export class App {
    configureRouter(config, router) {
        config.title = 'Plants Plants & Plants';
        config.map([
          { route: ['','plants'], name: 'plants', moduleId: './plants', nav: true, title:'Plants' },
          { route: 'edit/:id', name: 'edit', moduleId: './edit', nav: false, title:'Edit Plant' },
          { route: 'add', name: 'add', moduleId: './add', nav: true, title:'Add Plant' }
        ]);

        this.router = router;
    };
}