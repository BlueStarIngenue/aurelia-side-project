﻿import {inject} from 'aurelia-framework';
import {PlantService} from './plantService';
import {Router} from 'aurelia-router';

@inject(PlantService, Router)
export class EditPlant {
    plant = null;
    
     heading = 'Edit Plant';

    constructor(plantService, router) {
        this.plantService = plantService;
        this.router = router;
    }

    activate(params) {
        this.plant = this.plantService.getPlant(params.id);
    }

    update() {
        this.plantService.update(this.plant);
        this.router.navigate('plants');
    }
}