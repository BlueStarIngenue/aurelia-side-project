﻿import {inject} from 'aurelia-framework';
import {PlantService} from './plantService';

@inject(PlantService)
export class Plants {
    heading = 'Plants & More Plants';
    plants = [];

    constructor(plantService) {
        this.plantService = plantService;
    }

    activate() {
        this.plants = this.plantService.getPlants();
    }

    delete(id) {
        this.plantService.delete(id);
    }
}