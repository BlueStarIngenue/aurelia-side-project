﻿export class PlantService {

    plants = [];

    constructor() {

        this.plants.push({
            imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/5/54/Lithops_salicola.jpg',
            commonName: 'Living Stone',
            family: 'Aizoaceae',
            genus: 'Lithops',
            id: 1,
        });
        this.plants.push({
            imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/d/d9/Honeysuckle-1.jpg',
            commonName: 'Honeysuckle',
            family: 'Caprifoliaceae',
            genus: 'Lonicera',
            id: 2,
        });
        this.plants.push({
            imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/e/ed/Sacred_lotus_Nelumbo_nucifera.jpg',
            commonName: 'Lotus',
            family: 'Nelumbonaceae',
            genus: 'Nelumbo',
            id: 3,
        });
    }

    nextId = 4;

    getPlants() {
        return this.plants;
    }

    getPlant(id) {
        let plant = this.plants.find(plant => id == plant.id);
        return Object.assign({}, plant);
    }

    update(newPlant) {
        let plant = this.plants.find(plant => newPlant.id == plant.id);
        Object.assign(plant, newPlant);
    }

    add(newPlant) {
        let plant = {
            id: this.nextId++,
        };
        this.plants.push(Object.assign(plant, newPlant));
    }

    delete(id) {
        let index = this.plants.findIndex(plant => id == plant.id);
        this.plants.splice(index, 1);
    }
}